#!/usr/bin/env python3
import pprint
import requests
from dotenv import load_dotenv, dotenv_values
import os

load_dotenv()
HOST_NAME = os.getenv('PROXMOX_HOST')
SITE_NAME = os.getenv('SITE_NAME')
API_URL = f"http://{HOST_NAME}/{SITE_NAME}/check_mk/api/1.0"
print(API_URL)

USERNAME = os.getenv('PROXMOX_USER')
PASSWORD = os.getenv('PROXMOX_PASSWORD')

hostgroup = 'containers'
hostgroup_alias = 'container group'

session = requests.session()
session.headers['Authorization'] = f"Bearer {USERNAME} {PASSWORD}"
session.headers['Accept'] = 'application/json'

resp = session.post(
    f"{API_URL}/domain-types/host_group_config/collections/all",
    headers={
        "Content-Type": 'application/json',  # (required) A header specifying which type of content is in the request/response body.
    },
    json={'name': hostgroup, 'alias': hostgroup_alias},
)
if resp.status_code == 200:
    pprint.pprint(resp.json())
elif resp.status_code == 204:
    print("Done")
else:
    raise RuntimeError(pprint.pformat(resp.json()))