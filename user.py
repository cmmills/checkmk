#!/usr/bin/env python3
"""
Add a user to checkmk using the API
Documentation: http://<your_checmk_site/<site_name>/check_mk/openapi/#operation/cmk.gui.plugins.openapi.endpoints.user_config.create_user

"""
import pprint
import requests 
import argparse
from dotenv import load_dotenv, dotenv_values
import os


def create_session():
    load_dotenv()
    HOST_NAME = os.getenv('PROXMOX_HOST')
    SITE_NAME = os.getenv('SITE_NAME')
    API_URL = f"http://{HOST_NAME}/{SITE_NAME}/check_mk/api/1.0"
    print(API_URL)

    USERNAME = os.getenv('PROXMOX_USER')
    PASSWORD = os.getenv('PROXMOX_PASSWORD')

    session = requests.session()
    session.headers['Authorization'] = f"Bearer {USERNAME} {PASSWORD}"
    session.headers['Accept'] = 'application/json'
    return session, API_URL

def main(username, user_email, user_given_name, user_password, authorized_sites, user_role):
    session = create_session()
    session, API_URL = session[0], session[1]

    HOST_NAME = "10.0.0.146"
    SITE_NAME = "millsresidence"
    API_URL = f"http://{HOST_NAME}/{SITE_NAME}/check_mk/api/1.0"

    username = username
    user_email = user_email
    user_given_name = user_given_name
    user_password = user_password
    authorized_sites = [authorized_sites]
    user_role =  [user_role]              # the roles can be admin,user or guest https://docs.checkmk.com/latest/en/wato_user.html#roles

    resp = session.post(
        f"{API_URL}/domain-types/user_config/collections/all",
        headers={
            "Content-Type": 'application/json',  # (required) A header specifying which type of content is in the request/response body.
        },
        json={
            'username': username,
            'fullname': user_given_name,
            'auth_option': {
                'auth_type': 'password',
                'password': user_password
            },
            'disable_login': False,
            'contact_options': {
                'email': user_email 
            },
            'pager_address': '',
            'idle_timeout': {
                'option': 'global'
            },
            'roles': user_role,
            'authorized_sites': authorized_sites,
            'contactgroups': ['all'],
            'disable_notifications': {
                'disable': False
            },
            'language': 'en',
            'interface_options': {
                'interface_theme': 'dark'
            }
        },
    )

    if resp.status_code == 200:
        pprint.pprint(resp.json())
    elif resp.status_code == 204:
        print("Done")
    else:
        raise RuntimeError(pprint.pformat(resp.json()))

def remove_user(username):
    session = create_session()
    session, API_URL = session[0], session[1]

    resp = session.delete(
        f"{API_URL}/objects/user_config/{username}",
    )

    if resp.status_code == 200:
        pprint.pprint(resp.json())
    elif resp.status_code ==  204:
        print("User " + username + " deleted")
    else:
        raise RuntimeError(pprint.pformat(resp.json()))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="user.py", description="Add/remove a user to/from checkmk using the API")
    parser.add_argument("-user", nargs=6, metavar=("username", "user-email", "users-given-name", "user-password", "authorized-sites","user-role"), help="The users options to be added")
    parser.add_argument("-d", nargs=1, metavar=("username"), help="The username of the user to be removed")
    parser.add_argument("-v", action="version", version="%(prog)s 0.1.0")
    args = parser.parse_args()

    if args.user:
        username = args.user[0]
        user_email = args.user[1]
        user_given_name = args.user[2]
        user_password = args.user[3]
        authorized_sites = args.user[4]
        user_role = args.user[5]
        main(username, user_email, user_given_name, user_password, authorized_sites, user_role)
    elif args.d:
        username = args.d[0]
        remove_user(username)
    else:
        parser.print_help()