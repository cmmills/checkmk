#!/usr/bin/env python3
"""
Create and remove downtime for a host in Checkmk
DOCS: http://<checkmk-server-name_or_ip>/<checkmk_site_name>/check_mk/openapi/#operation/cmk.gui.plugins.openapi.endpoints.downtime.create_host_related_downtime

Options for setting downtime:
hostname: passed in from the command line - required
host_comment: passed in from the command line, this is the comment that will be displayed in the downtime
Time format both begin and end time must be in ISO 8601 format
- https://www.iso.org/iso-8601-date-and-time-format.html
example: 2023-04-09T22:42:00Zi is 9th April 2023 at 22:42:00 UTC time or 18:42:00 local time - start and end time are required
recur: "fixed" "hour" "day" "week" "second_week" "fourth_week" "weekday_start" "weekday_end" "day_of_month"
downtime_type: "host" "hostgroup" "host_by_query" - required 
duration: 0 - will set the downtime to start automatially regardless of wheteher theres' a problem on the host or not, 
              When set, the downtime does not begin automatically at a nominated time, but when a real problem status appears for the host. Consequencely, 
              the start_time/end_time is only the time window in which the scheduled downtime can begin. 
"""

import pprint
import requests
import argparse
from dotenv import load_dotenv, dotenv_values
import os

def create_session():
    load_dotenv()
    HOST_NAME = os.getenv('PROXMOX_HOST')
    SITE_NAME = os.getenv('SITE_NAME')
    API_URL = f"http://{HOST_NAME}/{SITE_NAME}/check_mk/api/1.0"
    print(API_URL)

    USERNAME = os.getenv('PROXMOX_USER')
    PASSWORD = os.getenv('PROXMOX_PASSWORD')

    session = requests.session()
    session.headers['Authorization'] = f"Bearer {USERNAME} {PASSWORD}"
    session.headers['Accept'] = 'application/json'
    return session, API_URL


def main(hostname, comment, start_time, end_time):
    session = create_session()
    session, API_URL = session[0], session[1]

    hostname = hostname
    host_comment = comment
    start_time = start_time                 # example: 2023-04-09T22:42:00Z
    end_time = end_time                     # example: 2023-04-12T23:59:59Z
    recur = "fixed"                         # fixed = does not reoccur
    down_type = "host"                      # This option can be host,hosgroup or host_by_query

    resp = session.post(
        f"{API_URL}/domain-types/downtime/collections/host",
        headers={
            "Content-Type": 'application/json',  # (required) A header specifying which type of content is in the request/response body.
        },
        # This schema has multiple variations. Please refer to the 'Payload' section for details.
        json={
            'start_time': start_time,
            'end_time': end_time,
            'recur': recur,
            'duration': 0,
            'comment': host_comment,
            'downtime_type': down_type,
            'host_name': hostname
        },
    )
    if resp.status_code == 200:
        pprint.pprint(resp.json())
    elif resp.status_code == 204:
        print("Downtime set for " + hostname + "")
    else:
        raise RuntimeError(pprint.pformat(resp.json()))
    
    
def remove_downtime(hostname):
    session = create_session()
    session, API_URL = session[0], session[1]

    resp = session.post(
    f"{API_URL}/domain-types/downtime/actions/delete/invoke",
    headers={
        "Content-Type": 'application/json',  # (required) A header specifying which type of content is in the request/response body.
    },
    # This schema has multiple variations. Please refer to
    # the 'Payload' section for details.
    json={'delete_type': delete_type, 'host_name': hostname},
    )
    if resp.status_code == 200:
        pprint.pprint(resp.json())
    elif resp.status_code == 204:
        print("Downtime removed for " + hostname + "")
    else:
        raise RuntimeError(pprint.pformat(resp.json()))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="create_downtime", description="Create and remove downtime for a host in Checkmk", 
                                     epilog="Checkmk create downtime script")
    parser.add_argument("-o", nargs=4, metavar=("hostname", "comment", "start time", "End time"), 
                        help="Pass in the hostname and comment for the downtime - the comment ")
    parser.add_argument("-r", nargs=1, type=str, help="hostname to be removed from downtime")
    parser.add_argument("-v", action="version", version="%(prog)s 0.1.0")
    args = parser.parse_args()

    if args.o:
        hostname = args.o[0]
        comment = args.o[1]
        start_time = str(args.o[2])
        end_time = str(args.o[3])
        main(hostname,comment,start_time,end_time)
    elif args.r:
        hostname = args.r
        remove_downtime(hostname)
    else:
        parser.print_help()